#include "dyn_array.h"

#include <stdlib.h>

struct DynArray
dynarray_create()
{
	struct DynArray tmp;
	tmp.size = 0;
	tmp.capacity = 0;
	tmp.data = 0;
	return tmp;
}

void
dynarray_free(struct DynArray* d)
{
	free(d->data);
	d->data = 0;
	d->size = 0;
	d->capacity = 0;
}

void
dynarray_push(struct DynArray *d, void* item)
{
	if (d->size >= d->capacity) {
		dynarray_reserve(d, (d->size + 1) * 2);
	}
	*(d->data + d->size++) = item;
}

void dynarray_pop(struct DynArray* d)
{
	d->size--;
	if (d->size < 0) {
		d->size = 0;
	}
}

void* dynarray_get(struct DynArray* d, int idx)
{
	return d->data[idx];
}

void dynarray_reserve(struct DynArray* d, int size)
{
	if (d->capacity > size) {
		return;
	}
	void** tmp = malloc(sizeof(void*) * size);
	if (d->data != 0) {
		for (int i = 0; i < d->size; i++) {
			tmp[i] = d->data[i];
		}
		free(d->data);
	}
	d->data = tmp;
	d->capacity = size;
}