#ifndef SHAPES_H
#define SHAPES_H

#include <stdbool.h>

#include "vector.h"
#include "material.h"

struct Sphere 
{
	struct Vec3f center;
	float radius;
	struct Material mat;
};

bool sphere_intersect(struct Sphere* s, struct Vec3f* origin, struct Vec3f* dir, float *t0);

#endif // SHAPES_H