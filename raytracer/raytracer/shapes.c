#include <stdbool.h>
#include <math.h>

#include "vector.h"
#include "shapes.h"

bool sphere_intersect(struct Sphere* s, struct Vec3f* origin, struct Vec3f* dir, float *t0)
{
	struct Vec3f L = vec3f_sub(&(s->center), origin);
	float tca = vec3f_mult(&L, dir);
	float d2 = vec3f_mult(&L, &L) - (tca * tca);
	if (d2 > s->radius * s->radius) {
		return false;
	}
	float thc = sqrtf((s->radius * s->radius) - d2);
	*t0 = tca - thc;
	float t1 = tca + thc;
	if (*t0 < 0) {
		*t0 = t1;
	}
	if (*t0 < 0) {
		return false;
	}
	return true;
}