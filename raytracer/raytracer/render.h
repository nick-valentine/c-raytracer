#ifndef RENDER_H
#define RENDER_H

#include <stdbool.h>

#include "vector.h"
#include "dyn_array.h"
#include "material.h"

struct Vec3f render_reflect(struct Vec3f* I, struct Vec3f* N);
struct Vec3f render_refract(struct Vec3f* I, struct Vec3f* N, float refractive_index);
bool render_scene_intersect(struct Vec3f* orig, struct Vec3f* dir, struct DynArray* spheres, struct Vec3f* hit, struct Vec3f* N, struct Material* material);
struct Vec3f render_cast_ray(struct Vec3f* origin, struct Vec3f* dir, struct DynArray* spheres, struct DynArray* lights, int depth);
void render(struct Vec3f* framebuffer, struct Vec2i size, float fov, struct DynArray* spheres, struct DynArray* lights);
void render_write(struct Vec3f* buffer, struct Vec2i size, char* target);

#endif // RENDER_H