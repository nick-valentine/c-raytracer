#ifndef DYN_ARRAY_H
#define DYN_ARRAY_H

struct DynArray {
	int size;
	int capacity;
	void** data;
};

struct DynArray dynarray_create();
void dynarray_free(struct DynArray* d);
void dynarray_push(struct DynArray* d, void* item);
void dynarray_pop(struct DynArray* d);
void* dynarray_get(struct DynArray* d, int idx);

void dynarray_reserve(struct DynArray* d, int size);

#endif // DYN_ARRAY_H