#include "vector.h"

#include <math.h>

struct Vec3f
vec3f_sub(struct Vec3f *a, struct Vec3f *b)
{
	struct Vec3f out;
	out.x = a->x - b->x;
	out.y = a->y - b->y;
	out.z = a->z - b->z;
	return out;
}

struct Vec3f 
vec3f_add(struct Vec3f* a, struct Vec3f* b)
{
	struct Vec3f out;
	out.x = a->x + b->x;
	out.y = a->y + b->y;
	out.z = a->z + b->z;
	return out;
}

float
vec3f_mult(struct Vec3f* a, struct Vec3f* b)
{
	float out = a->x * b->x;
	out += a->y * b->y;
	out += a->z * b->z;
	return out;
}

struct Vec3f 
vec3f_scale(struct Vec3f* a, float scalar)
{
	struct Vec3f out;
	out.x = a->x * scalar;
	out.y = a->y * scalar;
	out.z = a->z * scalar;
	return out;
}

struct Vec3f 
vec3f_negate(struct Vec3f* a)
{
	struct Vec3f out;
	out.x = -1 * a->x;
	out.y = -1 * a->y;
	out.z = -1 * a->z;
	return out;
}

struct Vec3f 
vec3f_normalize(struct Vec3f* a)
{
	float divisor = vec3f_len(a);
	struct Vec3f out;
	out.x = a->x / divisor;
	out.y = a->y / divisor;
	out.z = a->z / divisor;
	return out;
}

float 
vec3f_len(struct Vec3f* a)
{
	return sqrtf((a->x * a->x) + (a->y * a->y) + (a->z * a->z));
}