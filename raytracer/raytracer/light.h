#ifndef LIGHT_H
#define LIGHT_H

#include "vector.h"

struct Light
{
	struct Vec3f position;
	float intensity;
};

#endif // LIGHT_H