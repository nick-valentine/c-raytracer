﻿#include <stdio.h>
#include <stdlib.h>

#include "material.h"
#include "shapes.h"
#include "light.h"
#include "dyn_array.h"
#include "render.h"

int main()
{
	struct Material ivory;
	ivory.albedo.w = 0.6;
	ivory.albedo.x = 0.3;
	ivory.albedo.y = 0.1;
	ivory.albedo.z = 0.0;
	ivory.diffuse_color.x = 0.4;
	ivory.diffuse_color.y = 0.4;
	ivory.diffuse_color.z = 0.3;
	ivory.specular_exponent = 50;
	ivory.refractive_index = 1;

	struct Material glass;
	glass.albedo.w = 0.1;
	glass.albedo.x = 0.1;
	glass.albedo.y = 0.05;
	glass.albedo.z = 0.9;
	glass.diffuse_color.x = 0.6;
	glass.diffuse_color.y = 0.7;
	glass.diffuse_color.z = 0.8;
	glass.specular_exponent = 12;
	glass.refractive_index = 1;

	struct Material red_rubber;
	red_rubber.albedo.w = 0.9;
	red_rubber.albedo.x = 0.1;
	red_rubber.albedo.y = 0.0;
	red_rubber.albedo.z = 0.0;
	red_rubber.diffuse_color.x = 0.3;
	red_rubber.diffuse_color.y = 0.1;
	red_rubber.diffuse_color.z = 0.1;
	red_rubber.specular_exponent = 10;
	red_rubber.refractive_index = 1;

	struct Material mirror;
	mirror.albedo.w = 0.9;
	mirror.albedo.x = 0.10;
	mirror.albedo.y = 0.8;
	mirror.albedo.z = 0.0;
	mirror.diffuse_color.x = 0.1;
	mirror.diffuse_color.y = 0.1;
	mirror.diffuse_color.z = 0.1;
	mirror.specular_exponent = 1425;
	mirror.refractive_index = 1;

	struct DynArray spheres = dynarray_create();
	{
		struct Sphere tmp;
		tmp.center.x = -3;
		tmp.center.y = 0;
		tmp.center.z = -16;
		tmp.radius = 2;
		tmp.mat = ivory;
		dynarray_push(&spheres, &tmp);
	}
	{
		struct Sphere tmp;
		tmp.center.x = -1;
		tmp.center.y = -1.5;
		tmp.center.z = -12;
		tmp.radius = 2;
		tmp.mat = glass;
		dynarray_push(&spheres, &tmp);
	}
	{
		struct Sphere tmp;
		tmp.center.x = 1.5;
		tmp.center.y = -0.5;
		tmp.center.z = -18;
		tmp.radius = 3;
		tmp.mat = red_rubber;
		dynarray_push(&spheres, &tmp);
	}
	{
		struct Sphere tmp;
		tmp.center.x = 7;
		tmp.center.y = 5;
		tmp.center.z = -18;
		tmp.radius = 4;
		tmp.mat = mirror;
		dynarray_push(&spheres, &tmp);
	}

	struct DynArray lights = dynarray_create();
	{
		struct Light tmp;
		tmp.intensity = 1.5;
		tmp.position.x = -20;
		tmp.position.y = 20;
		tmp.position.z = 20;
		dynarray_push(&lights, &tmp);
	}
	{
		struct Light tmp;
		tmp.intensity = 1.8;
		tmp.position.x = 30;
		tmp.position.y = 50;
		tmp.position.z = -25;
		dynarray_push(&lights, &tmp);
	}
	{
		struct Light tmp;
		tmp.intensity = 1.7;
		tmp.position.x = 30;
		tmp.position.y = 20;
		tmp.position.z = -30;
		dynarray_push(&lights, &tmp);
	}

	struct Vec2i size;
	size.x = 1024;
	size.y = 728;

	struct Vec3f* framebuffer = malloc(sizeof(struct Vec3f) * (long)size.x * (long)size.y);

	render(framebuffer, size, 45.f, &spheres, &lights);

	render_write(framebuffer, size, "./out.bmp");

	free(framebuffer);
	dynarray_free(&lights);
	dynarray_free(&spheres);

	printf("done\n");

	return 0;
}
