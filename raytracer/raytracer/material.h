#ifndef MATERIAL_H
#define MATERIAL_H

#include "vector.h"

struct Material
{
	struct Vec4f albedo;
	struct Vec3f diffuse_color;
	float specular_exponent;
	float refractive_index;
};

#endif // MATERIAL_H