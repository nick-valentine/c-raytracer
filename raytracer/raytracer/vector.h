#ifndef VECTOR_H
#define VECTOR_H

struct Vec2i
{
	float x;
	float y;
};

struct Vec3f
{
	float x;
	float y;
	float z;
};

struct Vec3f vec3f_sub(struct Vec3f *a, struct Vec3f *b);
struct Vec3f vec3f_add(struct Vec3f* a, struct Vec3f* b);
float vec3f_mult(struct Vec3f *a, struct Vec3f *b);
struct Vec3f vec3f_scale(struct Vec3f* a, float scalar);
struct Vec3f vec3f_negate(struct Vec3f* a);
struct Vec3f vec3f_normalize(struct Vec3f* a);
float vec3f_len(struct Vec3f* a);

struct Vec4f
{
	float w;
	float x;
	float y;
	float z;
};

#endif // VECTOR_H