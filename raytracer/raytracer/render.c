#include "render.h"

#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <stdio.h>

#include "shapes.h"
#include "light.h"

const float GAMMA_CORRECTION = 0.8;
const float BRIGHTNESS_CORRECTION = 0.4;

struct Vec3f
render_reflect(struct Vec3f* I, struct Vec3f* N)
{
	float scalar = 2.0 * vec3f_mult(I, N);
	struct Vec3f scaled = vec3f_scale(N, scalar);
	return vec3f_sub(I, &scaled);
}

struct Vec3f
render_refract(struct Vec3f* I, struct Vec3f* N, float refractive_index)
{
	float cosi = -fmax(-1.0, fmin(1.0, vec3f_mult(I, N)));
	float etai = 1;
	float etat = refractive_index;

	struct Vec3f n;
	n.x = N->x;
	n.y = N->y;
	n.z = N->z;
	if (cosi < 0) {
		cosi = -cosi;
		etai = refractive_index;
		etat = 1;
		n = vec3f_negate(&n);
	}
	float eta = etai / etat;
	float k = 1 - eta * eta * (1 - cosi * cosi);
	if (k < 0) {
		struct Vec3f tmp;
		tmp.x = 0;
		tmp.y = 0;
		tmp.z = 0;
		return tmp;
	}

	float scalar = eta * cosi * sqrtf(k);
	struct Vec3f norm = vec3f_scale(&n, scalar);
	struct Vec3f i = vec3f_scale(I, eta);
	return vec3f_add(&norm, &i);
}

bool
render_scene_intersect(struct Vec3f* orig, struct Vec3f* dir, struct DynArray* spheres, struct Vec3f* hit, struct Vec3f* N, struct Material* material)
{
	float sphere_dist = FLT_MAX;
	for (int i = 0; i < spheres->size; i++) {
		float dist_i = FLT_MAX;
		struct Sphere* s = (struct Sphere*)dynarray_get(spheres, i);
		if (sphere_intersect(s, orig, dir, &dist_i)) {
			if (dist_i < sphere_dist) {
				sphere_dist = dist_i;
				struct Vec3f scaled = vec3f_scale(dir, dist_i);
				*hit = vec3f_add(orig, &scaled);
				struct Vec3f n = vec3f_sub(hit, &(s->center));
				*N = vec3f_normalize(&n);
				*material = s->mat;
			}
		}
	}

	float checkerboard_dist = FLT_MAX;
	if (fabs(dir->y) > 1e-3) {
		float d = -(orig->y + 4) / dir->y;
		struct Vec3f scaled = vec3f_scale(dir, d);
		struct Vec3f pt = vec3f_add(orig, &scaled);
		if (d > 0 && fabs(pt.x) < 10 && pt.z > -30 && d < sphere_dist) {
			checkerboard_dist = d;
			hit->x = pt.x;
			hit->y = pt.y;
			hit->z = pt.z;
			N->x = 0;
			N->y = 1;
			N->z = 0;
			if (((int)((int)(0.5 * hit->x + 1000.0) + (int)(0.5 * hit->z))) & 1) {
				material->albedo.w = 0.2;
				material->albedo.x = 0.2;
				material->albedo.y = 0.9;
				material->albedo.z = 0.0;
				material->diffuse_color.x = 0.2;
				material->diffuse_color.y = 0.2;
				material->diffuse_color.z = 0.2;
				material->specular_exponent = 0.1;
				material->refractive_index = 1.0;
			} else {
				material->albedo.w = 0.1;
				material->albedo.x = 0.3;
				material->albedo.y = 0.1;
				material->albedo.z = 0.0;
				material->diffuse_color.x = 0.1;
				material->diffuse_color.y = 0.3;
				material->diffuse_color.z = 0.1;
				material->specular_exponent = 0.1;
				material->refractive_index = 1.0;
			}
		}
		return fmin(sphere_dist, checkerboard_dist) < 60.0;
	}
}

struct Vec3f 
render_cast_ray(struct Vec3f* origin, struct Vec3f* dir, struct DynArray* spheres, struct DynArray* lights, int depth)
{
	float offset = 1e-3f;

	struct Vec3f point;
	struct Vec3f N;
	struct Material material;

	if (depth > 4 || !render_scene_intersect(origin, dir, spheres, &point, &N, &material)) {
		struct Vec3f out;
		out.x = 0.2;
		out.y = 0.2;
		out.z = 0.3;
		return out;
	}

	struct Vec3f reflect_dir = render_reflect(dir, &N);
	reflect_dir = vec3f_normalize(&reflect_dir);
	
	struct Vec3f reflect_origin;
	if (vec3f_mult(&reflect_dir, &N) < 0) {
		struct Vec3f scaled = vec3f_scale(&N, offset);
		reflect_origin = vec3f_sub(&point, &scaled);
	} else {
		struct Vec3f scaled = vec3f_scale(&N, offset);
		reflect_origin = vec3f_add(&point, &scaled);
	}

	struct Vec3f reflect_color = render_cast_ray(&reflect_origin, &reflect_dir, spheres, lights, depth + 1);

	struct Vec3f refract_dir = render_refract(dir, &N, material.refractive_index);
	refract_dir = vec3f_normalize(&refract_dir);
	struct Vec3f refract_orig;
	if (vec3f_mult(&refract_dir, &N) < 0) {
		struct Vec3f scaled = vec3f_scale(&N, offset);
		refract_orig = vec3f_sub(&point, &scaled);
	} else {
		struct Vec3f scaled = vec3f_scale(&N, offset);
		refract_orig = vec3f_add(&point, &scaled);
	}

	struct Vec3f refract_color = render_cast_ray(&refract_orig, &refract_dir, spheres, lights, depth + 1);

	float diffuse_light_intensity = 0;
	float specular_light_intensity = 0;

	for (int i = 0; i < lights->size; i++) {
		struct Light* it = (struct Light*)dynarray_get(lights, i);
		struct Vec3f light_dir = vec3f_sub(&(it->position), &point);
		float light_distance = vec3f_len(&light_dir);
		light_dir = vec3f_normalize(&light_dir);

		struct Vec3f shadow_orig;
		if (vec3f_mult(&light_dir, &N) < 0) {
			struct Vec3f scaled = vec3f_scale(&N, offset);
			shadow_orig = vec3f_sub(&point, &scaled);
		} else {
			struct Vec3f scaled = vec3f_scale(&N, offset);
			shadow_orig = vec3f_add(&point, &scaled);
		}

		struct Vec3f shadow_pt;
		struct Vec3f shadow_n;
		struct Material tmp_mat;
		if (render_scene_intersect(&shadow_orig, &light_dir, spheres, &shadow_pt, &shadow_n, &tmp_mat)) {
			struct Vec3f diff = vec3f_sub(&shadow_pt, &shadow_orig);
			float len = vec3f_len(&diff);
			if (len < light_distance) {
				continue;
			}
		}

		diffuse_light_intensity += it->intensity * fmax(0.0, vec3f_mult(&light_dir, &N));
		struct Vec3f neg_light_dir = vec3f_negate(&light_dir);
		struct Vec3f reflect = render_reflect(&neg_light_dir, &N);
		reflect = vec3f_negate(&reflect);
		specular_light_intensity += powf(fmax(0.0, vec3f_mult(&reflect, dir)), material.specular_exponent) * it->intensity;
	}

	struct Vec3f diffuse = vec3f_scale(&(material.diffuse_color), diffuse_light_intensity);
	struct Vec3f specular;
	specular.x = 1;
	specular.y = 1;
	specular.z = 1;
	specular = vec3f_scale(&specular, specular_light_intensity * material.albedo.w);
	specular.x += material.albedo.x;
	specular.y += material.albedo.x;
	specular.z += material.albedo.x;
	struct Vec3f reflect = vec3f_scale(&reflect_color, material.albedo.y);
	struct Vec3f refract = vec3f_scale(&refract_color, material.albedo.z);

	struct Vec3f sum = vec3f_add(&diffuse, &specular);
	sum = vec3f_add(&sum, &reflect);
	sum = vec3f_add(&sum, &refract);
	return sum;
}

void
render(struct Vec3f* framebuffer, struct Vec2i size, float fov, struct DynArray* spheres, struct DynArray* lights)
{
	for (int i = 0; i < size.y; i++) {
		for (int j = 0; j < size.x; j++) {
			float x = (2.0 * ((float)j + 0.5) / (float)size.x - 1) * (float)tan(fov / 2.0) * (float)size.x / (float)size.y;
			float y = -(2.0 * ((float)i + 0.5) / (float)size.y - 1) * (float)tan(fov / 2.0);
			struct Vec3f dir;
			dir.x = x;
			dir.y = y;
			dir.z = -1;
			dir = vec3f_normalize(&dir);
			struct Vec3f zero;
			zero.x = 0;
			zero.y = 0;
			zero.z = 0;
			int idx = j + (i * size.x);
			framebuffer[idx] = render_cast_ray(&zero, &dir, spheres, lights, 0);
		}
	}
}

void 
render_write(struct Vec3f* buffer, struct Vec2i size, char* target)
{
	unsigned int width = size.x;
	unsigned int height = size.y;

	unsigned char* output_format = malloc(sizeof(unsigned char) * width * height * 3);
	for (int i = 0; i < height * width; ++i) {
		struct Vec3f c = buffer[i];

		c = vec3f_scale(&c, BRIGHTNESS_CORRECTION);

		c.x = pow(c.x, (1.0 / GAMMA_CORRECTION));
		c.y = pow(c.y, (1.0 / GAMMA_CORRECTION));
		c.z = pow(c.z, (1.0 / GAMMA_CORRECTION));

		float largest = fmax(c.x, fmax(c.y, c.z));
		if (largest > 1.0) {
			c = vec3f_scale(&c, (1.0 / largest));
		}

		output_format[(i * 3) + 0] = (c.x * 255.0);
		output_format[(i * 3) + 1] = (c.y * 255.0);
		output_format[(i * 3) + 2] = (c.z * 255.0);
	}

	stbi_write_bmp(target, width, height, 3, output_format);
	free(output_format);
}
